//
//  BinaryOperator.h
//  Calculator
//
//  Created by Sutee Sudprasert on 2/17/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "Operator.h"
#import "Operand.h"

@interface BinaryOperator : Operator

- (double)computeWithLeftOperand:(Operand *)leftOperand andRightOperand:(Operand *)rightOperand;
- (id)init;

@end
