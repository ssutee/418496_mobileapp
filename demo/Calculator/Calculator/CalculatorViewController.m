//
//  CalculatorViewController.m
//  Calculator
//
//  Created by Sutee Sudprasert on 2/17/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "CalculatorViewController.h"
#import "Calculator.h"
#import "Operand.h"
#import "PlusOperator.h"
#import "BinaryMinusOperator.h"
#import "LeftParenthesisToken.h"
#import "RightParenthesisToken.h"

@interface CalculatorViewController()
{
    // instance variables 
    BOOL _shouldClearInputDisplay;
    BOOL _shouldClearStack;
}

@property (nonatomic, strong) Calculator *calculator;

- (void)enterInput;

@end

@implementation CalculatorViewController
@synthesize stackLabel = _stackLabel;
@synthesize inputLabel = _inputLabel;
@synthesize calculator = _calculator;

#pragma mark - Getter/Setter methods

- (Calculator *)calculator
{
    if (_calculator == nil) {
        _calculator = [[Calculator alloc] init];
        _calculator.delegate = self;
    }
    return _calculator;
}

#pragma mark - Private methods

- (void)enterInput
{
    if (self.inputLabel.text == nil || self.inputLabel.text.length == 0) {
        return;
    }
    [self.calculator pushToken:[[Operand alloc] initWithValue:[self.inputLabel.text doubleValue]]];
}

#pragma mark - Calculator delegate methods

- (void)calculator:(Calculator *)calculator didFinishComputeWithResult:(double)result
{
    self.inputLabel.text = [[NSString alloc] initWithFormat:@"%.2lf", result];
}

- (void)calculator:(Calculator *)calculator didFinishUpdateStack:(NSArray *)stack
{
    self.stackLabel.text = @"";
    for (Token *token in stack) {
        self.stackLabel.text = [self.stackLabel.text stringByAppendingFormat:@" %@", token];
    }
}

- (void)calculator:(Calculator *)calculator didFinishComputeWithError:(NSError *)error
{
    NSLog(@"error");
}

#pragma mark - Action methods

- (IBAction)pressNumberAction:(UIButton *)sender
{
    if (self.inputLabel.text == nil && sender.tag == 0) { return; } 
    
    if (self.inputLabel.text == nil) {
        self.inputLabel.text = [[NSString alloc] initWithFormat:@"%d", sender.tag];
    } else {
        self.inputLabel.text = [self.inputLabel.text stringByAppendingFormat:@"%d", sender.tag];
    }
}

- (IBAction)pressDotAction:(id)sender {
    if (self.inputLabel.text == nil) {
        self.inputLabel.text = @".";
        
    } else if ([self.inputLabel.text rangeOfString:@"."].location == NSNotFound) {
        self.inputLabel.text = [self.inputLabel.text stringByAppendingString:@"."];
    }
}

- (IBAction)pressPlusAction:(id)sender {
    [self enterInput];
    [self.calculator pushToken:[[PlusOperator alloc] init]];
}

#pragma mark - View lifecycle

- (void)viewDidUnload
{
    [self setInputLabel:nil];
    [self setStackLabel:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
