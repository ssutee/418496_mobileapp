package th.ac.ku.android.sutee.dotdot.fragment;

import java.util.Random;

import th.ac.ku.android.sutee.dotdot.fragment.Dots.OnDotsChangeListener;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

public class DotDotFragmentActivity extends FragmentActivity implements
		OnDotsChangeListener {

	private static final String TAG_LIST_FRAG = "TAG_LIST_FRAG";
	private static final String TAG_DRAW_FRAG = "TAG_DRAW_FRAG";
	
	private Dots dots = new Dots();
	private Random generator = new Random();

	private DotListFragment dotListFragment;
	private DotDrawFragment dotDrawFragment;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		FragmentManager fragManager = getSupportFragmentManager();
		FragmentTransaction xact = fragManager.beginTransaction();
		
		if (fragManager.findFragmentByTag(TAG_LIST_FRAG) == null) {
			dotListFragment = DotListFragment.createInstance(dots);
			xact.add(R.id.listFrameLayout, dotListFragment, TAG_LIST_FRAG);
		}
		
		if (fragManager.findFragmentByTag(TAG_DRAW_FRAG) == null) {
			dotDrawFragment = DotDrawFragment.createInstance(dots);
			xact.add(R.id.drawFrameLayout, dotDrawFragment, TAG_DRAW_FRAG);
		}
		
		xact.commit();

		dots.setOnDotsChangeListener(this);

	}

	@Override
	public void onDotsChange() {
		dotListFragment.updateViews();
		dotDrawFragment.updateViews();
		
	}

	public void randomDot(View view) {
		int x = generator.nextInt(200);
		int y = generator.nextInt(200);
		dots.addDot(x, y, Color.GRAY, 10, UIDUtils.getNextUID());
	}
}