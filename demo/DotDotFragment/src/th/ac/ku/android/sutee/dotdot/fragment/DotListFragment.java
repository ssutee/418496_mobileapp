package th.ac.ku.android.sutee.dotdot.fragment;

import th.ac.ku.android.sutee.dotdot.fragment.DotListAdapter.DotDataSource;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class DotListFragment extends Fragment {

	private static final String TAG_DOTS = "TAG_DOTS";
	private Dots dots;
	private ListView mListView;
	private DotListAdapter mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState == null) {
			savedInstanceState = getArguments();
		}

		if (savedInstanceState != null) {
			this.dots = savedInstanceState.getParcelable(TAG_DOTS);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.list_fragment, container, false);
		mListView = (ListView)view.findViewById(R.id.listView);
		mAdapter = new DotListAdapter(getActivity());
		mAdapter.setDotDataSource(new DotDataSource() {
			@Override
			public int size() {
				return dots.size();
			}
			
			@Override
			public Dot dot(int position) {
				return dots.getDot(position);
			}
		});
		mListView.setAdapter(mAdapter);
		return view;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelable(TAG_DOTS, this.dots);
	}

	public static DotListFragment createInstance(Dots dots) {
		Bundle init = new Bundle();
		init.putParcelable(TAG_DOTS, dots);
		DotListFragment fragment = new DotListFragment();
		fragment.setArguments(init);
		return fragment;
	}
	
	public void updateViews() {
		mAdapter.notifyDataSetChanged();
	}

}
