//
//  Photo+Flickr.m
//  FlickrFetcher
//
//  Created by Sutee Sudprasert on 3/26/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "Photo+Flickr.h"
#import "Photographer+Create.h"

@implementation Photo (Flickr)

+ (Photo *)photoWithFlickrInfo:(id)flickrInfo inManagedObjectContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photo"];
    request.predicate = [NSPredicate predicateWithFormat:@"uid = %@", [flickrInfo objectForKey:@"id"]];
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];

    Photo *photo = nil;    
    
    if (error || results.count > 1) {
        // handle error
        NSLog(@"%@", error.localizedDescription);
    } else if (results.count == 0) {
        photo = [NSEntityDescription insertNewObjectForEntityForName:@"Photo" inManagedObjectContext:context];    
        photo.url = [NSString stringWithFormat:@"http://farm%@.staticflickr.com/%@/%@_%@.jpg", [flickrInfo objectForKey:@"farm"], [flickrInfo objectForKey:@"server"], [flickrInfo objectForKey:@"id"], [flickrInfo objectForKey:@"secret"]];
        photo.title = [flickrInfo objectForKey:@"title"];
        photo.uid = [flickrInfo objectForKey:@"id"];   
        photo.owner = [Photographer photographerWithUID:[flickrInfo objectForKey:@"owner"] inManagedObjectContext:context];
        photo.created = [NSDate date];
    } else {
        photo = results.lastObject;
    }
    return photo;
}

@end
