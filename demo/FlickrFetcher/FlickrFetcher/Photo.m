//
//  Photo.m
//  FlickrFetcher
//
//  Created by Sutee Sudprasert on 3/26/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "Photo.h"
#import "Photographer.h"


@implementation Photo

@dynamic url;
@dynamic uid;
@dynamic title;
@dynamic created;
@dynamic owner;

@end
