//
//  PhotoViewController.h
//  FlickrFetcher
//
//  Created by Sutee Sudprasert on 3/12/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "DetailViewController.h"

@interface PhotoViewController : DetailViewController<SplitViewBarButtonItemPresenter>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) NSURL *url;
@property (weak, nonatomic) IBOutlet UIToolbar *myToolbar;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end
