//
//  Photographer.m
//  FlickrFetcher
//
//  Created by Sutee Sudprasert on 3/26/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "Photographer.h"
#import "Photo.h"


@implementation Photographer

@dynamic uid;
@dynamic photos;

@end
