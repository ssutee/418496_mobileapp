package th.ac.ku.android.sutee.dotdotlist;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "dot")
public class Dot {

	public static final String ID_COLUME = "_id";
	public static final String X_COLUME = "x";
	public static final String Y_COLUME = "y";

	@DatabaseField(generatedId = true, columnName = ID_COLUME)
	private int id;

	@DatabaseField(columnName = X_COLUME)
	private int x;

	@DatabaseField(columnName = Y_COLUME)
	private int y;

	Dot() {
	}
	
	public Dot(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getId() {
		return id;
	}
	
	
}
