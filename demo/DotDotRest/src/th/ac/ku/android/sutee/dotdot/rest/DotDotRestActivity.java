package th.ac.ku.android.sutee.dotdot.rest;

import java.util.Random;
import java.util.UUID;

import th.ac.ku.android.sutee.dotdot.rest.DotService.DotServiceBinder;
import th.ac.ku.android.sutee.dotdot.rest.DotService.DotServiceCallbackListener;
import android.R.color;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DotDotRestActivity extends FragmentActivity implements
		DotServiceCallbackListener, LoaderCallbacks<Cursor> {

	private static final String TAG = "DotDotRestActivity";

	// models
	private boolean isPending;

	// views
	private TextView pendingStatusText;
	ListView mListView;

	public final class DotCursorAdapter extends CursorAdapter {

		Context mContext;
		
		public final class ViewHolder {
			TextView coordXText;
			TextView coordYText;
			TextView statusText;
		}
		
		public DotCursorAdapter(Context context, Cursor c, int flags) {
			super(context, c, flags);
			mContext = context;
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			ViewHolder holder = new ViewHolder();
			View view = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
			holder.coordXText = (TextView)view.findViewById(R.id.coordXText);
			holder.coordYText = (TextView)view.findViewById(R.id.coordYText);
			holder.statusText = (TextView)view.findViewById(R.id.statusText);
			holder.statusText.setTextColor(Color.YELLOW);
			view.setTag(holder);
			return view;
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			ViewHolder holder = (ViewHolder)view.getTag();
			holder.coordXText.setText(cursor.getInt(cursor.getColumnIndex(Dot.X_COLUMN))+"");
			holder.coordYText.setText(cursor.getInt(cursor.getColumnIndex(Dot.Y_COLUMN))+"");
			
			if (cursor.getInt(cursor.getColumnIndex(Dot.STATE_POSTING_COLUMN)) == 1) {
				holder.statusText.setText("Status: posting");
				holder.coordXText.setTextColor(Color.DKGRAY);
				holder.coordYText.setTextColor(Color.DKGRAY);
			} else {
				holder.statusText.setText("");
				holder.coordXText.setTextColor(Color.GRAY);
				holder.coordYText.setTextColor(Color.GRAY);				
			}
		}
		
	}
	
	DotCursorAdapter mAdapter;

	private DotService mBindedService;
	private ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			if (mBindedService != null) {
				mBindedService.setCallbackListener(null);
				mBindedService = null;
			}
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mBindedService = ((DotServiceBinder) service).getService();
			mBindedService.setCallbackListener(DotDotRestActivity.this);
		}
	};

	public boolean isPending() {
		return isPending;
	}

	public void setPending(boolean isPending) {
		this.isPending = isPending;
		
		// update View
		if (isPending) {
			getPendingStatusText().setText(
					"Pending (" + DotServiceHelper.getRequestSize() + ")");
		} else {
			getPendingStatusText().setText("");
		}
	}

	public TextView getPendingStatusText() {
		if (pendingStatusText == null) {
			pendingStatusText = (TextView) findViewById(R.id.pendingStatusText);
		}
		return pendingStatusText;
	}

	private void updatePendingStatus() {
		if (DotServiceHelper.getRequestSize() > 0) {
			setPending(true);
		} else {
			setPending(false);
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		mAdapter = new DotCursorAdapter(this, null, 0);

		mListView = (ListView) findViewById(R.id.listView1);
		mListView.setAdapter(mAdapter);

		getSupportLoaderManager().initLoader(0, null, this);

		updatePendingStatus();
	}

	@Override
	protected void onStart() {
		super.onStart();
		bindService(new Intent(this, DotService.class), mConnection,
				Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onStop() {
		unbindService(mConnection);
		super.onStop();
	}

	@Override
	protected void onPause() {
		if (mBindedService != null) {
			mBindedService.setCallbackListener(null);
		}
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mBindedService != null) {
			mBindedService.setCallbackListener(this);
		}
		updatePendingStatus();
	}

	public void sync(View view) {
		String requestId = UUID.randomUUID().toString();
		DotServiceHelper.performGetOperation(this, requestId);
		updatePendingStatus();
	}

	private Random mGenerator = new Random();

	public void random(View view) {
		String requestId = UUID.randomUUID().toString();
		int x = mGenerator.nextInt(250);
		int y = mGenerator.nextInt(250);
		Dot dot = new Dot(x, y, requestId);
		DotServiceHelper.performPostOperation(this, requestId, dot, true);
		updatePendingStatus();
	}

	public void retry(View view) {
		DotServiceHelper.performPendingOperations(this);
		updatePendingStatus();
	}

	@Override
	public void onOperationComplete(String requestId) {
		updatePendingStatus();
		Toast.makeText(this, "Operation Complete", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "requestId = " + requestId + " is completed");
	}

	@Override
	public void onOperationFail(String requestId) {
		updatePendingStatus();
		Toast.makeText(this, "Operation Fail", Toast.LENGTH_SHORT).show();
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		return new CursorLoader(this, DotProvider.CONTENT_URI, null, null,
				null, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		mAdapter.swapCursor(data);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		mAdapter.swapCursor(null);
	}

}