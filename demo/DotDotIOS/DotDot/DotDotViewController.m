//
//  DotDotViewController.m
//  DotDot
//
//  Created by Sutee Sudprasert on 2/27/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "DotDotViewController.h"
#import "Dots.h"
#import "DotTableViewController.h"

@interface DotDotViewController()

@property (nonatomic, strong) Dots *model;

@end

@implementation DotDotViewController
@synthesize dotView = _dotView;

@synthesize model = _model;
@synthesize dotColor = _dotColor;

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"%d", buttonIndex);
}

- (Dots *)model
{
    if (_model == nil) {
        _model = [[Dots alloc] init];
        _model.delegate = self;
    }
    return _model;
}

- (IBAction)testAction:(id)sender {
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Title" message:@"Message" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    alertView.cancelButtonIndex = 0;
    [alertView show];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:segue.identifier]) {
        DotTableViewController *controller = segue.destinationViewController;
        controller.model = self.model;
    }
}

- (IBAction)tapOnDotViewAction:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateEnded) {
        NSLog(@"tap");
        [self.model addDot:[[Dot alloc] initWithOrigin:[sender locationInView:self.dotView]]];
    }
}

- (void) dotsDidChange:(NSArray *)dots
{
    NSLog(@"dotsDidChange");
    [self.dotView setNeedsDisplay];
}

- (NSArray *)dotItems
{
    NSLog(@"%@", self.model.dotItems);
    return self.model.dotItems;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.dotView.dotColor = self.dotColor;
}

- (void)viewDidUnload {
    [self setDotView:nil];
    [super viewDidUnload];
}
@end
