from django.core.urlresolvers import reverse
from djangorestframework.resources import ModelResource
from dotdot.dots.models import Dot

class DotResource(ModelResource):
    model = Dot
    fields = ('id', 'gid', 'x', 'y', 'radius', 'alpha', 'color', 'updated_at',)
    ordering = ('id',)
